package com.scorm.selenium.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookType;

public class ExcelReadWrite {

	public static ArrayList getValueFromExcel() throws IOException {
		String excelFilePath = BaseTest.getPath() + "//src//test//resources//testdata//VideoFiles.xlsx";
		ArrayList al = new ArrayList();
		int totalColumns;
		Row row;
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wb.getSheet("Sheet1");
		int totalRows = sheet.getLastRowNum();
		for (int i = 1; i <= totalRows; i++) {
			row = sheet.getRow(i);
			totalColumns = row.getLastCellNum();

			for (int y = 0; y < totalColumns; y++) {
				al.add(row.getCell(y).getStringCellValue());
			}
		}
		return al;
	}

	public static void deleteFile() {
		try {

			File file = new File(BaseTest.getPath() + "//src//test//resources//testdata//Result.xlsx");

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("File not exist.");
			}

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public static void createExcelFile() {
		try {
			String filename = BaseTest.getPath() + "//src//test//resources//testdata//Result.xlsx";
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Result");
			
			FileOutputStream fileOut = new FileOutputStream(filename);
			workbook.write(fileOut);
			fileOut.close();
			System.out.println("file created");
		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	public static void setValueInExcel(String moduleName, String status) throws IOException {
		try {
			File fileName = new File(BaseTest.getPath() + "//src//test//resources//testdata//Result.xlsx");
			
			FileInputStream ExcelFile = new FileInputStream(fileName);
            XSSFWorkbook workbook = new XSSFWorkbook(ExcelFile);
            XSSFSheet sheet = workbook.getSheet("Result");
            XSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("Module Name");
            row.createCell(1).setCellValue("Status");
            int lastRow= sheet.getLastRowNum();
            XSSFRow rowhead = sheet.createRow(lastRow+1);
            rowhead.createCell(0).setCellValue(moduleName);
            rowhead.createCell(1).setCellValue(status);
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();
	} catch (Exception e) {

		e.printStackTrace();

	}

	}

}
