package com.scorm.testscript;

import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.scorm.dataproviders.DataProviders;
import com.scorm.selenium.framework.BaseTest;
import com.scorm.selenium.framework.ExcelReadWrite;

import ch.qos.logback.core.encoder.EchoEncoder;

public class LoginLogout  extends BaseTest{

	
	@Factory(dataProvider = "Config", dataProviderClass = DataProviders.class)
	public LoginLogout(String browser) {
		super(browser);
		System.out.println(browser);
	}

	@Test(description = "LoginLogout")
	public void loginLogout() throws Exception {
		
		reportLog("Change appliation language to English");
		loginPage = loginPage.selectLanguage();
		
		reportLog("Login with user name " + userName + " and password " + password);
		dashBoardPage = loginPage.login(userName, password);

		reportLog("verify user login succesfully");
		dashBoardPage.verifyLoginSuccess();

		reportLog("Logout from application");
		loginPage = dashBoardPage.logOut();
	}
}
