package com.scorm.testscript;

import org.testng.Assert;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.scorm.dataproviders.DataProviders;
import com.scorm.pages.AdministratorMenuPage;
import com.scorm.selenium.framework.BaseTest;
import com.scorm.selenium.framework.Configuration;
import com.scorm.selenium.framework.Utilities;

public class VerifyCourseLesson extends BaseTest {
	AdministratorMenuPage administartorPage;
	String course, courseName, lessonType, shortDescription, description, packageName, resource, code;

	@Factory(dataProvider = "Config", dataProviderClass = DataProviders.class)
	public VerifyCourseLesson(String browser) {
		super(browser);
		System.out.println(browser);
	}

	@Test(description = "Verify course Lesson")
	public void verifyCourseLesson() throws Exception {
		String random = Utilities.randomString(4);
		course = Configuration.readApplicationFile("course");
		lessonType = Configuration.readApplicationFile("LessonType");
		courseName = Configuration.readApplicationFile("CourseName");
		shortDescription = Configuration.readApplicationFile("ShortDescription");
		description = Configuration.readApplicationFile("Description");
		resource = Configuration.readApplicationFile("Resource");
		code = Configuration.readApplicationFile("Code");
		packageName = Configuration.readApplicationFile("PackageName");
		
		reportLog("Change appliation language to English");
		loginPage = loginPage.selectLanguage();

		reportLog("Login with user name " + userName + " and password " + password);
		dashBoardPage = loginPage.login(userName, password);

		reportLog("verify user login succesfully");
		dashBoardPage.verifyLoginSuccess();

		reportLog("Click on Administrator Tab");
		administartorPage = dashBoardPage.clickOnAdministartorMenu();

		reportLog("Click on Course Link");
		administartorPage = administartorPage.clickOnCourseLink();

		reportLog("Find Course name");
		administartorPage = administartorPage.searchCourse(course);

		reportLog("Click on Modify button");
		administartorPage = administartorPage.clickOnModifyButton();

		reportLog("Click on Lessons");
		administartorPage = administartorPage.clickOnLessonLink();

		reportLog("Click on Modify button");
		administartorPage = administartorPage.clickOnModifyButton();

		reportLog("Enter Lesson Details");
		administartorPage = administartorPage.fillLessonDetails(courseName + random, lessonType, shortDescription);

		reportLog("Click on save button");
		administartorPage = administartorPage.clickOnSavebutton();

		reportLog("Verify Save lesson message");
		Assert.assertTrue(administartorPage.verifySaveMessage());

		reportLog("Click on Last breadcum");
		administartorPage = administartorPage.clickOnLastBreadcumLink();

		reportLog("Click on New Online Lesson");
		administartorPage = administartorPage.clickOnNewOnlineLessonLink();

		reportLog("Enter Lesson Details");
		administartorPage = administartorPage.fillLessonDetails(courseName + random, lessonType, shortDescription);

		reportLog("Select Package");
		administartorPage = administartorPage.selectPackage(packageName, resource);

		reportLog("Click on save button");
		administartorPage = administartorPage.clickOnSavebutton();

		reportLog("Verify Save lesson message");
		Assert.assertTrue(administartorPage.verifySaveMessage());

		reportLog("Click on Last breadcum");
		administartorPage = administartorPage.clickOnLastBreadcumLink();
		administartorPage = administartorPage.clickOnLastBreadcumLink();
		
		reportLog("Enter Lesson Details");
		administartorPage = administartorPage.fillCourseDetails(code+random, courseName + random, description);

		reportLog("Click on save button");
		administartorPage = administartorPage.clickOnSavebutton();

		reportLog("Logout from application");
		loginPage = dashBoardPage.logOut();
	}
}
